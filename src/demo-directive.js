(function(){

	angular.module('demoDirective').directive('demoDirective',DemoDirective);
	DemoDirective.$inject = [];
	
	
	function DemoDirective(){
	
		require('./demo-directive.css');
	
		var directive = {
			restrict: 'E',
			template: require('./demo-directive.html'),
			link: Link,
			controller: Controller
		
		};
		
		function Link(scope,element,attrs){
		
		}
		
		function Controller($scope){
		
		}
		
		return directive;
	});
})();
